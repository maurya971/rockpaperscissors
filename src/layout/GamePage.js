import React from "react";

const GamePage = ({ tag: Tag, children, ...restProps }) => {
  return (
    <div {...restProps} className="card game-page-card align-center text-white">
      <div className="card-body">{children}</div>
    </div>
  );
};

export default GamePage;
