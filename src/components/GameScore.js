import React, { Component } from "react";
import GamePage from "../layout/GamePage";
import paper from "../assets/paper.png";
import rock from "../assets/rock.png";
import scissors from "../assets/scissors.png";
import paper_d from "../assets/paper_d.png";
import rock_d from "../assets/rock_d.png";
import scissors_d from "../assets/scissors_d.png";
import PRSContext from "../context/PRS-context";

class GameScore extends Component {
  static contextType = PRSContext;

  render() {
    let computerName = "";
    let resultText = "";
    debugger;
    if (!this.context.playerVsComputer) {
      computerName = "Computer 2 ";
    }
    if (this.context.gameResult === "wins") {
      resultText = computerName + "YOU WON!!";
    } else if (this.context.gameResult === "losses") {
      resultText = computerName + "YOU LOOSE!";
    } else if (this.context.gameResult === "draws") {
      resultText = "DRAW!";
    } else if (this.context.gameResult === "missed") {
      resultText = computerName + "You missed it, Play again!";
    } else {
      resultText = "Game not started!";
    }

    return (
      <GamePage>
        <div className="game-mode-options">
          <div className="row">
            {this.context.computerChoice === "paper" && (
              <div className="col-sm">
                <img
                  className="paper-button rps-base"
                  src={paper_d}
                  alt="Paper"
                />
              </div>
            )}
            {this.context.computerChoice === "rock" && (
              <div className="col-sm">
                <img
                  className="paper-button rps-base"
                  src={rock_d}
                  alt="Rock"
                />
              </div>
            )}
            {this.context.computerChoice === "scissors" && (
              <div className="col-sm">
                <img
                  className="paper-button rps-base"
                  src={scissors_d}
                  alt="Scissors"
                />
              </div>
            )}
          </div>
        </div>
        <div className="game-mode-options_middle">
          <h5>{resultText}</h5>
          <div className="row">
            <div className="col-sm">
              <button
                type="button"
                className="btn btn-warning btn-sm"
                onClick={this.context.startOver}
              >
                Play again!
              </button>
            </div>
            <div className="col-sm">
              <button
                type="button"
                className="btn btn-warning btn-sm"
                onClick={this.context.togglePlayerMode}
              >
                Change mode
              </button>
            </div>
          </div>
        </div>

        <div>
          <p />
          <div className="row">
            <div className="col-sm">
              <img
                className={
                  "paper-button rps-base " +
                  (this.context.userChoice === "paper" ? "" : "icon-opacity")
                }
                src={paper}
                alt="Paper"
              />
            </div>
            <div className="col-sm">
              <img
                className={
                  "paper-button rps-base " +
                  (this.context.userChoice === "rock" ? "" : "icon-opacity")
                }
                src={rock}
                alt="Rock"
              />
            </div>
            <div className="col-sm">
              <img
                className={
                  "paper-button rps-base " +
                  (this.context.userChoice === "scissors" ? "" : "icon-opacity")
                }
                src={scissors}
                alt="Scissors"
              />
            </div>
          </div>
        </div>
      </GamePage>
    );
  }
}

export default GameScore;
