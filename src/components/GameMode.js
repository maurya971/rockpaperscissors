import React, { Component } from "react";
import GamePage from "../layout/GamePage";
import PRSContext from "../context/PRS-context";

class GameMode extends Component {
  static contextType = PRSContext;

  render() {
    return (
      <GamePage>
        <p>Select play mode</p>
        <div className="game-mode-options">
          <button
            type="button"
            className="btn btn-warning"
            disabled={this.props.playerVsComputer}
            onClick={this.context.togglePlayerMode}
          >
            Player VS Computer
          </button>
        </div>
        <div className="game-mode-options">
          <button
            type="button"
            className="btn btn-warning"
            disabled={!this.props.playerVsComputer}
            onClick={this.context.togglePlayerMode}
          >
            Computer VS Computer
          </button>
        </div>
      </GamePage>
    );
  }
}

export default GameMode;
