import React, { Component } from "react";
import GamePage from "../layout/GamePage";
import paper from "../assets/paper.png";
import rock from "../assets/rock.png";
import scissors from "../assets/scissors.png";
import PRSContext from "../context/PRS-context";
//import Timer from "./Timer";

class GameBoard extends Component {
  static contextType = PRSContext;

  render() {
    let player1Name = this.props.playerVsComputer ? "Computer" : "Computer 1";
    let palyer2Name = this.props.playerVsComputer ? "Player" : "Computer 2";
    return (
      <GamePage>
        <div className="game-mode-options">
          <div className="circle-Base">{this.context.time}</div>
        </div>
        <div className="game-mode-options_middle">
          <h5>{player1Name}</h5>
          <hr className="game-board-seprator" />
          <h5>{palyer2Name}</h5>
        </div>
        {this.props.playerVsComputer && (
          <div>
            <p>Choose your move</p>
            <div className="row">
              <div className="col-sm">
                <img
                  className="paper-button rps-base"
                  src={paper}
                  alt="Paper"
                  onClick={() => this.props.onPlayerChoiceClick("paper")}
                />
              </div>
              <div className="col-sm">
                <img
                  className="paper-button rps-base"
                  src={rock}
                  alt="Rock"
                  onClick={() => this.props.onPlayerChoiceClick("rock")}
                />
              </div>
              <div className="col-sm">
                <img
                  className="paper-button rps-base"
                  src={scissors}
                  alt="Scissors"
                  onClick={() => this.props.onPlayerChoiceClick("scissors")}
                />
              </div>
            </div>
          </div>
        )}
      </GamePage>
    );
  }
}

export default GameBoard;
