import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import GameMode from "../components/GameMode";
import GamePage from "../layout/GamePage";

configure({ adapter: new Adapter() });

describe("GameMode", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<GameMode />);
  });

  it("should exist", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should have one GamePage component", () => {
    expect(wrapper.find(GamePage).length).toEqual(1);
  });

  it("should have 2 immediate div element of GamePage", () => {
    expect(wrapper.find("GamePage>div").length).toEqual(2);
  });

  it("should have 2 button component GamePage", () => {
    expect(wrapper.find("button").length).toEqual(2);
  });
});
