import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

//import CurrentGame from '../components/CurrentGame/CurrentGame';
//import {renderCurrentGameResult} from '../components/CurrentGame/CurrentGame'

import GamePage from "../layout/GamePage";
import { renderGamePageResult } from "../layout/GamePage";

configure({ adapter: new Adapter() });

describe("GamePageTests", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<GamePage />);
  });

  it("should exist", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should return the proper game page text as per input", () => {
    wrapper = shallow(<GamePage tag="div" />);
    expect(wrapper.html()).toEqual(
      '<div class="card game-page-card align-center text-white"><div class="card-body"></div></div>'
    );
  });
});
