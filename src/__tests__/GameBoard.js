import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import GameBoard from "../components/GameBoard";
import GamePage from "../layout/GamePage";

configure({ adapter: new Adapter() });

describe("GameMode", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<GameBoard />);
  });

  it("should exist", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should have one GamePage component", () => {
    expect(wrapper.find(GamePage).length).toEqual(1);
  });

  it("should have 2 immediate div element of GamePage if playerVsComputer is false", () => {
    expect(wrapper.find("GamePage>div").length).toEqual(2);
  });

  it("should have 3 immediate div element of GamePage if playerVsComputer is true", () => {
    wrapper = shallow(<GameBoard playerVsComputer="true" />);
    expect(wrapper.find("GamePage>div").length).toEqual(3);
  });

  it("should have 0 img element of GamePage if playerVsComputer is false", () => {
    wrapper = shallow(<GameBoard />);
    expect(wrapper.find("img").length).toEqual(0);
  });

  it("should have 3 img element of GamePage if playerVsComputer is true", () => {
    wrapper = shallow(<GameBoard playerVsComputer="true" />);
    expect(wrapper.find("img").length).toEqual(3);
  });
});
