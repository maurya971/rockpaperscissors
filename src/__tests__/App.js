import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import App from "../App";
import GameMode from "../components/GameMode";
import GameBoard from "../components/GameBoard";
import GameScore from "../components/GameScore";

configure({ adapter: new Adapter() });

describe("App", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  it("should exist", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should have the app state", () => {
    expect(wrapper.state().userChoice).toEqual(null);
    expect(wrapper.state().computerChoice).toEqual(null);
    expect(wrapper.state().gameResult).toEqual(null);
    expect(wrapper.state().playerVsComputer).toEqual(true);
    expect(wrapper.state().time).toEqual(0);
    expect(wrapper.state().isOn).toEqual(false);
  });

  it("should have a onPlayerChoiceClick method", () => {
    expect(wrapper.instance().onPlayerChoiceClick).toBeTruthy();
  });
  it("should have a startTimer method", () => {
    expect(wrapper.instance().startTimer).toBeTruthy();
  });

  it("should have a stopTimer method", () => {
    expect(wrapper.instance().stopTimer).toBeTruthy();
  });

  it("should have a startOver method", () => {
    expect(wrapper.instance().startOver).toBeTruthy();
  });

  it("should have a togglePlayerMode method", () => {
    expect(wrapper.instance().togglePlayerMode).toBeTruthy();
  });

  it("should have a render method", () => {
    expect(wrapper.instance().render).toBeTruthy();
  });

  it("should return something from render", () => {
    expect(wrapper.instance().render()).toBeTruthy();
  });

  it("should have a div element with class row", () => {
    expect(wrapper.find(".row").type()).toEqual("div");
  });

  it("should have only div element with class row", () => {
    expect(wrapper.find(".row").length).toEqual(1);
  });

  it("should have 3 column of main div with class row", () => {
    expect(wrapper.find(".row").find(".col-sm").length).toEqual(3);
  });

  it("should have an container class name", () => {
    expect(wrapper.hasClass("container")).toBeTruthy();
  });

  it("should have the GameMode component", () => {
    expect(wrapper.find(GameMode).length).toEqual(1);
  });

  it("should have the GameBoard component", () => {
    expect(wrapper.find(GameBoard).length).toEqual(1);
  });

  it("should have the GameScore component", () => {
    expect(wrapper.find(GameScore).length).toEqual(1);
  });
});
