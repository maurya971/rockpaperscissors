import React from "react";

export default React.createContext({
  userChoice: null,
  computerChoice: null,
  gameResult: null,
  playerVsComputer: true,
  time: 0,
  startOver: () => {},
  togglePlayerMode: () => {}
});
