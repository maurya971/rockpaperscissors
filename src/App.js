/**
 * @author Ashutosh
 */
import React, { Component } from "react";
import GameMode from "./components/GameMode";
import GameBoard from "./components/GameBoard";
import GameScore from "./components/GameScore";
import PRSContext from "./context/PRS-context";
import { getRandomChoice, getWinner } from "./helper/GameHelper";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userChoice: null,
      computerChoice: null,
      gameResult: null,
      playerVsComputer: true,
      time: 0,
      isOn: false
    };
    this.onPlayerChoiceClick = this.onPlayerChoiceClick.bind(this);
    this.startOver = this.startOver.bind(this);
    this.togglePlayerMode = this.togglePlayerMode.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.stopTimer = this.stopTimer.bind(this);
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.time === 5) {
      this.stopTimer();
      if (!this.state.playerVsComputer) {
        this.onPlayerChoiceClick(null);
      }
    }
  }
  /**
   * Handle user choice click handler, based on the player mode it will decide whether to choose user chice
   * or take both choice randon.
   * It will stop timer and set choice and result to state and context.
   * @param {*} choice -one of the three possible choices - paper, rock, scissors selected by the user
   */
  onPlayerChoiceClick(choice) {
    if (!this.state.isOn) {
      return;
    }
    this.stopTimer();
    let computerChoice = getRandomChoice();
    if (!this.state.playerVsComputer) {
      choice = getRandomChoice();
    }
    const result = getWinner(choice, computerChoice);

    this.setState({
      userChoice: choice,
      computerChoice: computerChoice,
      gameResult: result
    });
  }
  /**
   * Function will start timer
   */
  startTimer() {
    this.setState({
      isOn: true,
      time: this.state.time
    });
    this.timer = setInterval(
      () =>
        this.setState({
          time: this.state.time + 1
        }),
      1000
    );
  }
  /**
   * Function will stop timer, set time to 0
   * it will check if there is no game result untill count 5 will set result to missed
   */
  stopTimer() {
    this.setState({
      time: 0,
      isOn: false
    });
    clearInterval(this.timer);
    if (!this.state.gameResult) {
      this.setState({
        gameResult: "missed"
      });
    }
  }
  /**
   * Funtion will reset game to initial state
   */
  startOver() {
    this.setState({
      userChoice: null,
      computerChoice: null,
      gameResult: null
    });
    this.startTimer();
  }

  /**
   * Function will toggle the game mode between player vs computer and computer vs computer
   */
  togglePlayerMode() {
    this.setState((state, props) => {
      return { playerVsComputer: !state["playerVsComputer"] };
    });
    this.setState({
      userChoice: null,
      computerChoice: null,
      gameResult: null
    });
  }

  render() {
    return (
      <div className="container">
        <PRSContext.Provider
          value={{
            userChoice: this.state.userChoice,
            computerChoice: this.state.computerChoice,
            gameResult: this.state.gameResult,
            startOver: this.startOver,
            playerVsComputer: this.state.playerVsComputer,
            togglePlayerMode: this.togglePlayerMode,
            time: this.state.time
          }}
        >
          <div className="row">
            <div className="col-sm">
              <GameMode playerVsComputer={this.state.playerVsComputer} />
            </div>
            <div className="col-sm">
              <GameBoard
                onPlayerChoiceClick={this.onPlayerChoiceClick}
                playerVsComputer={this.state.playerVsComputer}
              />
            </div>
            <div className="col-sm">
              <GameScore />
            </div>
          </div>
        </PRSContext.Provider>
      </div>
    );
  }
}

export default App;
